use indexmap::IndexMap;
use ron::{
	de::from_reader,
	ser::{to_writer_pretty, PrettyConfig},
};
use serde::{Deserialize, Serialize};
use std::{
	env, fs,
	io::{BufRead, BufReader},
	process::exit,
};

const USAGE: &str = "\
$NAME $VERSION
$AUTHORS
$DESCRIPTION

USEAGE:
    $BIN_NAME morph
    $BIN_NAME obj reference

ARGS:
    <morph>        Head morph to convert
    <obj>          Wavefront OBJ to convert
    <reference>    Reference head morph for converting Wavefront OBJ

OPTIONS:
    -h, --help       Print help information
";

fn main() {
	let args = env::args().skip(1).collect::<Vec<_>>();
	if ![1, 2].contains(&args.len()) {
		exit_usage(1);
	} else if ["-h", "--help"].contains(&args[0].as_str()) {
		exit_usage(0);
	} else if args.len() == 1 {
		morph2obj(&args[0]);
	} else {
		obj2morph(&args[0], &args[1]);
	}
}

fn exit_usage(code: i32) -> ! {
	let full_usage = {
		let bin_name = env::args()
			.next()
			.unwrap_or_else(|| env!("CARGO_BIN_NAME").to_string());
		USAGE
			.replace("$NAME", env!("CARGO_PKG_NAME"))
			.replace("$VERSION", env!("CARGO_PKG_VERSION"))
			.replace("$AUTHORS", env!("CARGO_PKG_AUTHORS"))
			.replace("$DESCRIPTION", env!("CARGO_PKG_DESCRIPTION"))
			.replace("$BIN_NAME", &bin_name)
	};
	if code == 0 {
		print!("{}", full_usage);
	} else {
		eprint!("{}", full_usage);
	}
	exit(code);
}

#[derive(Serialize, Deserialize)]
struct Vertex {
	x: f64,
	y: f64,
	z: f64,
}

#[derive(Serialize, Deserialize)]
struct HeadMorph {
	hair_mesh: String,
	accessory_mesh: Vec<String>,
	morph_features: IndexMap<String, f64>,
	offset_bones: IndexMap<String, Vertex>,
	lod0_vertices: Vec<Vertex>,
	lod1_vertices: Vec<Vertex>,
	lod2_vertices: Vec<Vertex>,
	lod3_vertices: Vec<Vertex>,
	scalar_parameters: IndexMap<String, f64>,
	vector_parameters: IndexMap<String, [f64; 4]>,
	texture_parameters: IndexMap<String, String>,
}

fn morph2obj(morph: &str) {
	let morph_data: HeadMorph = {
		let morph_file = fs::File::open(morph).unwrap_or_else(|e| {
			eprintln!("Failed to open morph: {}\n", e);
			exit_usage(1)
		});

		from_reader(morph_file).unwrap_or_else(|e| {
			eprintln!("Invalid head morph file: {}", e);
			exit(1);
		})
	};

	for v in morph_data.lod0_vertices {
		println!("v {} {} {}", v.x, v.y, v.z);
	}
}

fn obj2morph(obj: &str, reference: &str) {
	let verts = {
		let obj_file = fs::File::open(obj).unwrap_or_else(|e| {
			eprintln!("Failed to open obj: {}\n", e);
			exit_usage(1)
		});

		let buf_reader = BufReader::new(obj_file);

		buf_reader
			.lines()
			.map(|l| {
				l.unwrap_or_else(|e| {
					eprintln!("Failed to read OBJ file: {}", e);
					exit(1);
				})
			})
			.filter_map(|l| {
				l.strip_prefix("v").map(|vert_str| {
					let components = vert_str
						.split_ascii_whitespace()
						.map(str::parse::<f64>)
						.collect::<Result<Vec<_>, _>>()
						.unwrap_or_else(|e| {
							eprintln!("Failed to read OBJ file vertex: {}", e);
							exit(1);
						});
					match components[..] {
						[x, y, z] => Vertex { x, y, z },
						[x, y, z, w] => Vertex {
							x: x / w,
							y: y / w,
							z: z / w,
						},
						_ => {
							eprintln!("Invalid wavefront OBJ file");
							exit(1);
						}
					}
				})
			})
			.collect::<Vec<_>>()
	};

	let mut reference_data: HeadMorph = {
		let reference_file = fs::File::open(reference).unwrap_or_else(|e| {
			eprintln!("Failed to open reference: {}\n", e);
			exit_usage(1)
		});

		from_reader(reference_file).unwrap_or_else(|e| {
			eprintln!("Invalid reference head morph file: {}", e);
			exit(1);
		})
	};

	if verts.len() != reference_data.lod0_vertices.len() {
		eprintln!(
			"OBJ file and reference head morph have a different number of vertices: {} != {}",
			verts.len(),
			reference_data.lod0_vertices.len(),
		)
	}

	reference_data.lod0_vertices = verts;

	to_writer_pretty(
		std::io::stdout(),
		&reference_data,
		PrettyConfig::new().enumerate_arrays(true),
	)
	.unwrap_or_else(|e| {
		eprintln!("Failed to serialize head morph: {}", e);
		exit(1);
	});
}
